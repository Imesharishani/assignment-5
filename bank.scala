class account(id:String,aNumber:Int,balance:Double)
{
	var nic=id;
	var accNumber=aNumber;
	var aBalance=balance;

	def withdraw(amount:Double)=if(aBalance>amount) aBalance=aBalance-amount else println("Your account balnce is not sufficient.");
	def deposit(amount:Double)=aBalance=aBalance+amount;
	def transfer(acc:List[account],amount:Double)=
	{
		if(aBalance>amount)
		{
			aBalance=aBalance-amount;
			acc.head.aBalance=acc.head.aBalance+amount;
		}
		else
		{
			println("Your account balnce is not sufficient.");
		}
	}	
}

object bank
{
	def main(args:Array[String])
	{
		
		var acc1=new account("B1111",1110,10000);
		var acc2=new account("B1112",1111,20000);
		val bank:List[account]=List(acc1,acc2);
		
		val x=find(1111,bank);
		acc1.transfer(x,5000);

		println("Transferd account balance : "+acc1.aBalance);
		println("Received account balance  : "+x.head.aBalance);
		
	}

	val find=(accNumber:Int,list:List[account])=>list.filter(x=>x.accNumber.equals(accNumber));

}
