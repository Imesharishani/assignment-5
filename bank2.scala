class account(id:String,aNumber:Int,balance:Double)
{
	var nic=id;
	var accNumber=aNumber;
	var aBalance=balance;
}

object bank2
{
	def main(args:Array[String])
	{

		var acc1=new account("B111",1111,30000);
		var acc2=new account("B112",1112,0);
		var acc3=new account("B113",1113,80000);
		var acc4=new account("B114",1114,-2000);
		var acc5=new account("B115",1115,-15000);
		val bank:List[account]=List(acc1,acc2,acc3,acc4,acc5);
		
		//Overdraft account numbers
		print("Overdraft account numbers   : ");
		var ODlist=overdraft(bank);
		ODlist.foreach(x=>print(x.accNumber+"   "));

		//Total account balance
		var tBalance=balance(bank);
		print("\nTotal account balance       : "+tBalance.aBalance);

		//Account balance after adding Balance-Interest
		print("\nAccount balances + interest : ")
		var interestList=interest(bank);
		interestList.foreach(x=>print(x+" "));

	}

	val overdraft=(list:List[account])=>list.filter(x=>x.aBalance<=0);
	val balance=(list:List[account])=>list.reduce((x,y)=>new account("S000",1000,x.aBalance+y.aBalance));
	val interest=(list:List[account])=>list.map(x=>(if(x.aBalance>0) x.aBalance*1.05d else x.aBalance*1.01d));
}
