class Rational(x:Int,y:Int)
{

  def numer=x
      def denom=y
      
      def sub(s:Rational)= new Rational(numer * s.denom - s.numer * denom, denom * s.denom)
}
object sub {
    def main(args:Array[String])
    {
     
    val p=new Rational(3,4)
    val q=new Rational(5,8)
   
    print(p.sub(q).numer)
    print("/")
    println(p.sub(q).denom)

    println("")
    val s=new Rational(p.sub(q).numer,p.sub(q).denom)
    val r=new Rational(2,7)
    
    println("Final answer is: ")
    print(s.sub(r).numer)
    print("/")
    println(s.sub(r).denom)
    
    
  }
}